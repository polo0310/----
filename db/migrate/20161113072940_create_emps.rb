class CreateEmps < ActiveRecord::Migration[5.0]
  def change
    create_table :emps do |t|
      t.string :empId
      t.string :empName
      t.string :salary

      t.timestamps
    end
  end
end
