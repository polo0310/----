Rails.application.routes.draw do
  resources :list, :only => [ :list ]
  get 'list/list'
  get 'login/login'
  root 'login#login'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
